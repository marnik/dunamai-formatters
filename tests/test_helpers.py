from dunamai_formatters import helpers


def test_format_pieces_as_pep440__only_base():
    assert helpers.format_pieces_as_pep440("1.2.3") == "1.2.3"
