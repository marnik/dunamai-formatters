# Contributing

Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

You can contribute in many ways:

## Types of Contributions

### Report Bugs

Report bugs at [https://gitlab.com/marnik/dunamai-formatters/-/issues](https://gitlab.com/marnik/dunamai-formatters/-/issues).

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

### Fix Bugs

Look through the GitLab issues for bugs. Anything tagged with "bug" and "help
wanted" is open to whoever wants to implement it.

### Implement Features

Look through the GitLab issues for features. Anything tagged with "enhancement"
and "help wanted" is open to whoever wants to implement it.

### Write Documentation

Dunamai Formatters could always use more documentation, whether as part of the
official Dunamai Formatters docs, in docstrings, or even on the web in blog posts,
articles, and such.

### Submit Feedback

The best way to send feedback is to file an issue at
[https://gitlab.com/marnik/dunamai-formatters/-/issues](https://gitlab.com/marnik/dunamai-formatters/-/issues).

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

## Get Started!

Ready to contribute? Here's how to set up `dunamai-formatters` for local development.

1. Fork the `dunamai-formatters` repo on GitLab.
2. Clone your fork locally

    ```shell
    $ git clone git@gitlab.com:your_name_here/dunamai-formatters.git
    ```

3. Install your local copy. Assuming you have poetry installed,
   this is how you set up your fork for local development

    ```shell
    $ cd dunamai-formatters/
    $ poetry install --with dev,quality,test,docs
    ```

   By using poetry a virtual environment is automatically managed.

4. Create a branch for local development

    ```shell
    $ git checkout -b name-of-your-bugfix-or-feature
    ```

    Now you can make your changes locally.

5. When you're done making changes, apply the code style and check that your changes pass
   all linters and the tests, including testing other Python versions with tox

    ```shell
    $ make linters
    $ make checkers
    $ make test
    $ make tox
    ```

    Normally every dependency should be installed. If not run `poetry install --with dev,quality,test,docs`

6. Commit your changes and push your branch to GitLab

    ```shell
    $ git add .
    $ git commit -m "Your detailed description of your changes."
    $ git push origin name-of-your-bugfix-or-feature
    ```

7. Submit a pull request through the GitLab website.

## A note about commits

This repo uses a commit message style loosely based on [Conventional Commits](https://www.conventionalcommits.org/en).
A key concept is making use of different types. Find a list of allowed types below. An extensive document detailing
commits can be found in [commits.md](commits.md)

### Type

* **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
* **ci**: Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs)
* **chore**: Needed repetitive tasks, but with no impact on the code.
* **docs**: Documentation only changes
* **feat**: A new feature
* **fix**: A bug fix
* **perf**: A code change that improves performance
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **revert**: Used when the commit reverts a previous commit.
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* **test**: Adding missing tests or correcting existing tests

## Pull Request Guidelines

Before you submit a pull request, check that it meets these guidelines:

1. The pull request should include tests.
2. If the pull request adds functionality, the docs should be updated. Put
   your new functionality into a function with a docstring, and add the
   feature to the list in README.md.
3. The pull request should work for Python 3.5, 3.6, 3.7 and 3.8, and for PyPy. Check
   [https://gitlab.com/marnik/dunamai-formatters/-/pipelines](https://gitlab.com/marnik/dunamai-formatters/-/pipelines)
   and make sure that the tests pass for all supported Python versions.

## Tips

To run a subset of tests::

```shell
$ poetry run pytest tests/test_helpers.py
```


## Publish

A reminder for the maintainers on how to publish.
Make sure your working directory is clean and checkout the candidate commit.

```shell
$ git checkout <hash>
```

If the code looks good and all necessary files are up to date you can tag the commit
and upload the tag.

```shell
$ git tag -s -m "Release v<version>" "v<version>"
$ git push --tags
```

GitLab CI will then deploy to PyPI if tests pass.
