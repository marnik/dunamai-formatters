# Tutorial

Try this out

```python
import dunamai
import dunamai_formatters

version = dunamai.Version(
    "0.3.0",
    epoch=2,
    stage=("alpha", 3),
    distance=7,
    commit="b6a9020",
    dirty=True,
    tagged_metadata="linux",
)

print(version.serialize())
print(version.serialize(format=dunamai_formatters.pep440.meta_id))
```

and you should see::

```shell
2!0.3.0a3.post7.dev0+b6a9020
2!0.3.0a3+d7.gb6a9020.dirty.linux
```
