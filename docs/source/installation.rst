.. highlight:: shell

============
Installation
============


Stable release
--------------

To install dunamai-formatters, run this command in your terminal:

.. code-block:: console

    $ pip install dunamai-formatters

This is the preferred method to install dunamai-formatters, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Dunamai Formatters can be downloaded from the `GitLab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git@gitlab.com:marnik/dunamai-formatters.git

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://gitlab.com/marnik/dunamai-formatters/-/archive/main/dunamai-formatters-main.tar

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _GitLab repo: https://gitlab.com/marnik/dunamai-formatters
.. _tarball: https://gitlab.com/marnik/dunamai-formatters/-/archive/main/dunamai-formatters-main.tar
