# Usage

This project provides formatters that can be used as the format argument of the `serialize`
function in [Dunamai](https://github.com/mtkennerly/dunamai).

You can use Dunamai Formatters by importing it into your Python application.

## Example

A sample usage would be

```python
import dunamai
import dunamai_formatters

__version__ = dunamai.get_version("<your package name>").serialize(
    format=dunamai_formatters.pep440.meta_id
)
```

## Provided Formatters

Currently, there are only pep440 formatters

## Creating your own formatters

The package contains helper functions to easily format Dunamai Versions.

```python
import dunamai
from dunamai_formatters import helpers


def my_custom_formatter(version: dunamai.Version) -> str:
    return helpers.format_pieces_as_pep440(version.base)
```
