Welcome to Dunamai Formatters documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   usage
   tutorial
   api
   contributing
   commits
   changelog

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Feedback
========

If you have any suggestions or questions about **dunamai-formatters** feel free to email me
at marnik@mdebont.be.

If you encounter any errors or problems with **dunamai-formatters**, please let me know!

Code Coverage Report
====================

`HTML report <./coverage/index.html>`_
