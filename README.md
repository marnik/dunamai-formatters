# Dunamai formatters
[![pipeline status](https://gitlab.com/marnik/dunamai-formatters/badges/dev/pipeline.svg)](https://gitlab.com/marnik/dunamai-formatters/-/commits/dev)
[![coverage report](https://gitlab.com/marnik/dunamai-formatters/badges/dev/coverage.svg)](https://gitlab.com/marnik/dunamai-formatters/-/commits/dev)
[![codecov](https://codecov.io/gl/marnik/dunamai-formatters/branch/dev/graph/badge.svg?token=TWKBXWKOUC)](https://codecov.io/gl/marnik/dunamai-formatters)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Security: bandit](https://img.shields.io/badge/security-bandit-yellow.svg)](https://github.com/PyCQA/bandit)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)

A complementary tool to [Dunamai](https://github.com/mtkennerly/dunamai) that offers formatters that can be used as the format argument of the `serialize` function.

## Features

* PEP440 formatters


