.PHONY: clean-build clean-pyc clean-test clean-coverage clean-docs check-bandit check-black check-flake8 check-ssort check-isort check-mypy check-pylint check-safety checkers lint-ssort lint-black lint-isort linters isort test test-all view-coverage changelog changelog-all changelog-next docs view-docs serve-docs build publish install
.DEFAULT_GOAL := help

define BROWSER_PYSCRIPT
import os, webbrowser, sys

from urllib.request import pathname2url

webbrowser.open("file://" + pathname2url(os.path.abspath(sys.argv[1])))
endef
export BROWSER_PYSCRIPT

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

BROWSER := poetry run python -c "$$BROWSER_PYSCRIPT"

version              := $(shell poetry run python -c 'import dunamai_formatters; print(dunamai_formatters.__version__)')
next-version         := $(shell poetry run python -c 'import dunamai_formatters, dunamai; print(dunamai.get_version("dunamai-formatters", third_choice=dunamai.Version.from_any_vcs, ignore=[dunamai.Version("0")], parser=dunamai.Version.parse).serialize(format=dunamai_formatters.pep440.meta_id, bump=True))')
version_pattern      = (?P<version>v?(?:(?:(?P<epoch>[0-9]+)!)?(?P<release>[0-9]+(?:\.[0-9]+)*)(?P<pre>[-_\.]?(?P<pre_l>(a|b|c|rc|alpha|beta|pre|preview))[-_\.]?(?P<pre_n>[0-9]+)?)?(?P<post>(?:-(?P<post_n1>[0-9]+))|(?:[-_\.]?(?P<post_l>post|rev|r)[-_\.]?(?P<post_n2>[0-9]+)?))?(?P<dev>[-_\.]?(?P<dev_l>dev)[-_\.]?(?P<dev_n>[0-9]+)?)?)(?:\+(?P<local>[a-z0-9]+(?:[-_\.][a-z0-9]+)*))?)
description          = A complementary tool to Dunamai that offers formatters that can be used as the format argument of the serialize function.
auto-changelog-extra =
#auto-changelog-extra = "--debug"

ssort-command-line = dunamai_formatters/ tests/
isort-command-line = --profile black --skip-glob docs --gitignore --combine-star --balanced --honor-noqa --color --atomic

help:
	@poetry run python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

clean: clean-build clean-pyc clean-test clean-coverage clean-docs ## remove all build, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	-rm -fr build/
	-rm -fr dist/
	-rm -fr .eggs/
	-find . -name '*.egg-info' -exec rm -fr {} +
	-find . -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	-find . -name '*.pyc' -exec rm -f {} +
	-find . -name '*.pyo' -exec rm -f {} +
	-find . -name '*~' -exec rm -f {} +
	-find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test artifacts
	-rm -fr .tox/
	-rm -fr .pytest_cache
	-rm -fr .mypy_cache
	-rm test_report.xml

clean-coverage: ## remove coverage artifacts
	poetry run coverage erase
	-rm -fr coverage/

clean-docs: ## remove docs artifacts
	$(MAKE) -C docs clean
	-rm -fr docs/build
	-rm -fr docs/source/api

check-ssort: ## check sorting of python files with ssort
	poetry run ssort $(ssort-command-line) --check --diff

check-isort: ## check imports with isort
	poetry run isort . -c $(isort-command-line)

check-black: ## check the black code formatter
	poetry run black . --check

check-bandit: ## check security with bandit
	poetry run bandit --ini .bandit -c bandit.yml

check-flake8: ## check style with flake8
	poetry run flake8

check-mypy: ## check style with mypy
	poetry run mypy

check-pylint: ## check style with pylint
	poetry run pylint dunamai_formatters

check-safety: ## check dependencies for known security vulnerabilities
	poetry export -f requirements.txt --without-hashes | poetry run safety check --stdin

checkers: check-ssort check-isort check-black check-bandit check-flake8 check-mypy check-pylint check-safety ## run all quality checkers

lint-ssort: ## run the ssort python formatter
	poetry run ssort $(ssort-command-line)

lint-isort: ## run the isort imports formatter
	poetry run isort . $(isort-command-line)

lint-black: ## run the black code formatter
	poetry run black .

linters: lint-ssort lint-isort lint-black ## run all quality linters

isort: ## run the isort imports formatter and let imports float to the top
	poetry run isort . $(isort-command-line) --float-to-top

test: ## run tests and code coverage quickly with the default Python
	poetry run pytest

test-all: ## run tests on every Python version with tox
	poetry run tox

view-coverage: test ## check code coverage quickly with the default Python
	$(BROWSER) coverage/html/index.html

changelog: ## create the changelog based on the git history
	poetry run auto-changelog --tag-pattern "$(version_pattern)" -d "$(description)" $(auto-changelog-extra)

changelog-all: ## create the changelog based on the git history and the latest commit 'released'
	poetry run auto-changelog --tag-pattern "$(version_pattern)" -d "$(description)" -u -v "$(version)" $(auto-changelog-extra)

changelog-next: ## create the changelog based on the git history and the latest commit 'released' as the next version
	poetry run auto-changelog --tag-pattern "$(version_pattern)" -d "$(description)" -u -v "$(next-version)" $(auto-changelog-extra)

docs: changelog clean-docs test ## generate Sphinx HTML documentation, including coverage
	$(MAKE) -C docs html
	cp -R coverage/html docs/build/html/coverage

view-docs: docs ## Open Sphinx HTML documentation, including API docs
	$(BROWSER) docs/build/html/index.html

serve-docs: view-docs ## compile the docs watching for changes
	poetry run watchmedo shell-command -w -p '*.rst;*.md' -c '$(MAKE) -C docs html' -R -D .

build: docs clean-build ## builds source and wheel package
	poetry build
	ls -l dist

publish: build ## package and upload a release
	poetry publish -r testpypi

install: clean ## install the package to the active Python's site-packages
	poetry install
